package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"time"

	color "github.com/fatih/color"
	yaml "gopkg.in/yaml.v2"
)

var (
	maxCities          = 5
	totalCitiesVisited = 0
	currentLocation    = "Patmos"
	boldMagenta        = color.New(color.FgMagenta, color.Bold).SprintFunc()
)

type Cities struct {
	Cities []City `yaml:"cities"`
}
type City struct {
	Name  string   `yaml:"name"`
	Clues []string `yaml:"clues"`
}
type People struct {
	People []string `yaml:"people"`
}

func main() {

	c := loadCitiesFromFile()
	rand.Seed(time.Now().UnixNano())
	// shuffle the order of cities
	orderedCities := shuffleCities(c.Cities)
	p := loadPeopleFromFile()
	people := shufflePeople(p.People)

	if len(orderedCities)-4 < maxCities {
		maxCities = len(orderedCities) - 4
	}
	clearScrean()
	fmt.Println("The apostle John has given you a letter to deliver to Gaius, but...")
	fmt.Println()
	time.Sleep(2 * time.Second)
	fmt.Println("Gaius left Patmos a few months ago and John doesn't have any idea where he is...")
	fmt.Println()
	time.Sleep(2 * time.Second)
	fmt.Println("Maybe some of the local brothers and sisters know where he went.")
	fmt.Println()
	time.Sleep(2 * time.Second)

	startInCity(people, orderedCities)
}

func startInCity(people []string, orderedCities []City) {
	localPeople, people := setLocalPeople(people)

	nextCity := orderedCities[0]
	orderedCities = orderedCities[1:len(orderedCities)]
	possibleCities := getFourCities(nextCity, orderedCities)

	for {
		color.Cyan("Select a city to go to, or talk to the local brothers:")
		fmt.Println()
		time.Sleep(1 * time.Second)

		color.Magenta("0) Talk to the local brothers")
		i := 0
		for _, element := range possibleCities {
			i++
			color.Cyan("%v) %v\n", i, element.Name)
		}
		fmt.Println()

		var choice int
		fmt.Scan(&choice)
		// if the number is outside the range, ask again
		for choice > len(possibleCities) {
			fmt.Println("?")
			fmt.Scan(&choice)
		}
		if choice == 0 {
			askForClues(nextCity, localPeople)
		} else {
			goToCity(possibleCities[choice-1], nextCity, people, orderedCities)
		}
	}
}
func goToCity(city City, nextCity City, people []string, orderedCities []City) {
	previousCity := currentLocation
	currentLocation = city.Name
	clearScrean()
	color.Green("After a long journey you arrive at %v\n\n", city.Name)
	time.Sleep(1 * time.Second)
	if city.Name == nextCity.Name {
		totalCitiesVisited++
		if maxCities-totalCitiesVisited < 1 {
			fmt.Println("Congratulations! You found Gaius. He thanks you for the letter.")
			os.Exit(0)
		} else if maxCities-totalCitiesVisited < 2 {
			fmt.Println("You just missed him! He left just a few HOURS ago!")
		} else if maxCities-totalCitiesVisited < 3 {
			fmt.Println("Getting really close. A few days ago he was here!")
		} else if maxCities-totalCitiesVisited < 4 {
			fmt.Println("Getting closer, he was here several days ago.")
		} else if maxCities-totalCitiesVisited < 5 {
			fmt.Println("You're on the right track. He was here, but left over a week ago.")
		} else {
			fmt.Println("Gauius was here a while ago... the local brothers might know where he went.")
		}
		fmt.Println()
		time.Sleep(1 * time.Second)
		startInCity(people, orderedCities)
	} else {
		fmt.Println("Hmm, it seams that Gauius has not been here, back to where you came from.")
		fmt.Println()
		time.Sleep(1 * time.Second)
		fmt.Println("Press enter to go back.")
		fmt.Scanln()
		currentLocation = previousCity
		clearScrean()
	}
}
func setLocalPeople(people []string) ([]string, []string) {
	// remove the first 3 people from the list and use them as locals
	return people[0:3], people[3:len(people)]
}
func askForClues(city City, localPeople []string) {
	clearScrean()
	color.Magenta("Who would you like to consult?")
	fmt.Println()
	time.Sleep(1 * time.Second)

	for i := 0; i < len(localPeople); i++ {
		color.Magenta("%v) %v\n", i+1, localPeople[i])
		time.Sleep(500 * time.Millisecond)
	}
	var person int
	fmt.Scan(&person)
	// if the number is outside the range, ask again
	for person < 1 || person > len(localPeople) {
		fmt.Println("?")
		fmt.Scan(&person)
	}
	clearScrean()
	fmt.Printf("%v: %v\n\n", boldMagenta(localPeople[person-1]), city.Clues[person-1])
	time.Sleep(1 * time.Second)
}

func clearScrean() {
	cmd := exec.Command("clear") //Linux only
	cmd.Stdout = os.Stdout
	cmd.Run()
	color.Yellow("Current location: %v\n\n", currentLocation)
}

func getFourCities(nextCity City, orderedCities []City) []City {
	// shuffle
	shuffled := shuffleCities(orderedCities)
	var possibleCities []City
	// select the first three results
	possibleCities = append(possibleCities, shuffled[0])
	possibleCities = append(possibleCities, shuffled[1])
	possibleCities = append(possibleCities, shuffled[2])
	// add back the next city
	possibleCities = append(possibleCities, nextCity)
	// reshuffle the four possible cities
	possibleCities = shuffleCities(possibleCities)
	return possibleCities
}

func shuffleCities(cities []City) []City {
	rand.Shuffle(
		len(cities),
		func(i, j int) { cities[i], cities[j] = cities[j], cities[i] },
	)
	return cities
}
func shufflePeople(people []string) []string {
	rand.Shuffle(
		len(people),
		func(i, j int) { people[i], people[j] = people[j], people[i] },
	)
	return people
}

func loadPeopleFromFile() People {
	var people People

	yamlFile, err := ioutil.ReadFile("people.yaml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &people)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return people
}

func loadCitiesFromFile() Cities {
	cities := Cities{}
	yamlFile, err := ioutil.ReadFile("cities.yaml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &cities)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return cities
}
