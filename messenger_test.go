package main

import (
	// "fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShufflePeople(t *testing.T) {
	given := []string{
		"Kyle",
		"Steve",
		"Isaiah",
		"Eliu",
		"Luis",
		"Jayson",
		"Emmanuel",
	}
	// take a snapshot of the slice because the function will
	// change the passed in slices' underlying array
	snapshot := []string{
		given[0],
		given[1],
		given[2],
		given[3],
		given[4],
		given[5],
		given[6],
	}
	got := shufflePeople(given)

	assert.NotEqual(t, snapshot, got)
}

func TestShuffleCities(t *testing.T) {
	given := []City{
		City{
			Name: "Fishkill",
		},
		City{
			Name: "Patterson",
		},
		City{
			Name: "Walkill",
		},
		City{
			Name: "Warwick",
		},
		City{
			Name: "Patterson",
		},
		City{
			Name: "Ft Lauderdale",
		},
		City{
			Name: "Chinle",
		},
	}
	// take a snapshot of the slice because the function will
	// change the passed in slices' underlying array
	snapshot := []City{
		given[0],
		given[1],
		given[2],
		given[3],
		given[4],
		given[5],
		given[6],
	}
	got := shuffleCities(given)

	assert.NotEqual(t, snapshot, got)
}

func TestGetFourCities(t *testing.T) {
	given := []City{
		City{
			Name: "Fishkill",
		},
		City{
			Name: "Patterson",
		},
		City{
			Name: "Walkill",
		},
		City{
			Name: "Warwick",
		},
		City{
			Name: "Patterson",
		},
		City{
			Name: "Ft Lauderdale",
		},
	}
	nextCity := City{
		Name: "Chinle",
	}
	got := getFourCities(nextCity, given)

	want := []City{
		City{
			Name: "Chinle",
		},
		City{
			Name: "Walkill",
		},
		City{
			Name: "Warwick",
		},
		City{
			Name: "Fishkill",
		},
	}
	assert.Equal(t, 4, len(got))
	assert.Equal(t, want[0], nextCity)
}

func TestSetLocalPeople(t *testing.T) {
	given := []string{
		"Kyle",
		"Steve",
		"Isaiah",
		"Eliu",
		"Luis",
		"Jayson",
		"Emmanuel",
	}
	gotLocal, gotRest := setLocalPeople(given)

	want := []string{
		"Kyle",
		"Steve",
		"Isaiah",
	}
	assert.Equal(t, 3, len(gotLocal))
	assert.Equal(t, 4, len(gotRest))
	assert.Equal(t, want, gotLocal)
}
